import fileinput


class FileService:

    def __init__(self, project_directory):
        self.project_directory = project_directory

#    def create_directory(self, dir_name):
#        try:
#            os.makedirs(dir_name)
#        except OSError:
#            if not os.path.isdir(dir_name):
#                raise

    def find_and_replace(self, files, test_to_find, text_to_replace):

        replacement_files_with_dir = []

        for file_name in files:
            replacement_files_with_dir.append(self.project_directory + "/" + file_name)

        f = fileinput.input(files=replacement_files_with_dir, inplace=True)

        for line in f:
            print(line.replace(test_to_find, text_to_replace))

        f.close()
