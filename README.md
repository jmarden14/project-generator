# Project Generator #

Welcome to project gen the idea behind this project is that it is a configurable tool to aid with generating projects based on a
template folder of your project. By supplying a few simple key value pairs in the yaml you can easily create projects with this
tool.

This tool was built because based on research I couldn't find a multi language tool which could generate a project of mine. It was frustrating when it came to renaming elements in configuration files and such. I noticed a number of JS tools like Yeoman but nothing for Java, Python etc.

I chose python for this tool as I believe it is a good language which will run on a large number of platforms without installing tons of dependencies.

Supplied in the project are the following files and folders to help you on your way:

* **example.yaml** - The configuration to set up the tool
* **resources folder** - The resources to copy over

Below is the example Yaml explained:


```
#!yaml

application: # This is the main object which will influence how the tools info will be shown to the user
  type: "Spring boot simple setup" # The type of the project
  info: "Welcome to an example project generation project" # Any additional information you want to supply to the user
  copyFolderName: "resources" # The folder which contains your template project

replaces: # An array of find/replaces
  - original-text: "test" # Text to replace 
    replacement-text: "test2" # The replacement text
    files: ["build/something.txt"] # Files where the replace will occur
  - riginal-text: "test"
    replacement-text: "test3"
    files: ["example.txt"]
```
    
The user of the project generation tool will be asked to input the following:

- Location of the project
- Name of the project

There are a number of features I want add and hopefully they will be available soon. The main goal is pulling a github or bitbucket project and running the generation tool against it.