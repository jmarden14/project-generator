import yaml
import os
import shutil
import logging
from services.file_service import FileService

dir_path = os.path.dirname(os.path.realpath(__file__))

logging.info("Current directory: %s",  dir_path)

yamlFileAndLocation = raw_input("Enter the location and name of your yaml file: ")

with open(yamlFileAndLocation, 'r') as f:
    doc = yaml.load(f)

applicationContext = doc["application"]
replaces = doc["replaces"]
copyFolderName = applicationContext["copyFolderName"]

print "Project Type: " + applicationContext["type"]
print "==================================================="
print applicationContext["info"]

projectLocation = raw_input("Where shall your project be saved: ")
applicationName = raw_input("Enter your application name: ")

print "==================================================="

projectDirectory = projectLocation + applicationName

fileService = FileService(projectDirectory)

logging.info("Creating project directory: %s", projectDirectory)

shutil.copytree(dir_path + "/" + copyFolderName, projectDirectory)

for replace in replaces:
    fileService.find_and_replace(replace["files"], replace["original-text"], replace["replacement-text"])
